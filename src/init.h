/*
    init.h: Define some macros and configurations for tipsi and Eigen.

    Author: Kaixiang Huang <kxhuang@whu.edu.cn>.
    Copyright (c) 2019, Group Tipsi.

    All rights reserved. Use of this source code is governed by a
    ???? license that can be found in the LICENSE file.
*/

#ifdef MKL // enable blas, lapack, fftw via MKL
    #define EIGEN_USE_MKL_VML
#endif


#define EIGEN_NO_DEBUG  // disable debug features
