#include "tbpm.hh"
#include "sample.hh"
#include "utils.hh"


namespace tipsi{ namespace internal {

// get correlation of DOS using tbpm
CVector tbpm_dos(const SMatrix& H_csr, Real rescaled_range,
                 Idx seed, Idx n_timestep, Idx n_ran_samples)
{
    using namespace math;
    using namespace constant;
    using namespace ios;

    const Idx n_wf = H_csr.n_wf();
    CVector wf_in(n_wf), wf(n_wf);
    Complex corrval;
    CVector corr(n_timestep);

    ofstream file;
    file.open("corr_DOS.dat");
    file << " Number of samples   =" << setw(8) << n_ran_samples << '\n';
    file << " Number of timesteps =" << setw(8) << n_timestep    << '\n';
    file << std::setprecision(12) << std::scientific;

    Real x = 2 * PI / rescaled_range;
    RVector bes = get_Bes(x);

    H_csr.optimize(n_ran_samples*(bes.size()-1));

    // iterate over n_ran_samples
    for (Idx i_sample = 0; i_sample < n_ran_samples; i_sample++) {
        file << "  Sample =" << setw(8) << i_sample + 1 << '\n';
        cout << "  Sample" << setw(8) << i_sample + 1 << "  of"
             << setw(8) << n_ran_samples << endl;

        wf_in = random_state(n_wf, seed*i_sample);

        // first t_step = 1 here
        wf = cheb_wf_timestep_fwd(wf_in, bes, H_csr);
        corrval = inner_prod(wf_in, wf);

        file << setw(8) << '1' << "  "
             << setw(20) << corrval.real() << ' '
             << setw(20) << corrval.imag() << '\n';

        corr[0] += corrval / n_ran_samples;

        for (Idx t_step = 2; t_step < n_timestep + 1; t_step++) {
            if (t_step % 64 == 0) {
                cout << "    Timestep" << setw(8) << t_step
                     <<"  of" << setw(8) << n_timestep << endl;
                file << flush;
            }
            wf = cheb_wf_timestep_fwd(wf, bes, H_csr);
            corrval = inner_prod(wf_in, wf);

            file << setw(8) << t_step << "  "
                 << setw(20) << corrval.real() << ' '
                 << setw(20) << corrval.imag() << '\n';

            corr[t_step - 1] += corrval / n_ran_samples;
        }
    }
    file.close();

    return corr;
}

}} // namespace tipsi::internal
