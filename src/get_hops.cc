#include "sample.hh"
#include <omp.h>

namespace tipsi {

void Sample::get_hoppings(const HopDict& hopdict)
{
    using namespace internal;

    #pragma omp parallel
    {
        Idx n_threads = omp_get_num_threads();
        Idx chunk = 1 + ((n_wf-1) / n_threads);
        Idx j_tag, orb0;
        Loc3 loc0, loc1;
        SMatrix_div csr(hopdict.size(), chunk);

        #pragma omp for schedule(static, chunk)
        for (Idx i_tag = 0; i_tag < n_wf; i_tag++) {
            const SiteTag& tag0 = siteset.tag(i_tag);
            loc0 = tag0.loc();
            orb0 = tag0.orb();
            const SDict& hop_dict = hopdict[orb0];

            for (auto&& item: hop_dict) {
                const RelTag& rel_tag = item.first;

                j_tag = siteset.get_idx(loc0, rel_tag);
                if (j_tag == n_wf) continue;

                const Complex& hop = item.second;

                csr.add(j_tag, hop);
            }
            csr.next();
        }

        #pragma omp for ordered schedule(static, 1)
        for (Idx i = 0; i < n_threads; i++) {
            #pragma omp ordered
            H_csr.concatenate(csr);
        }
    }
}

} // namespace tipsi
