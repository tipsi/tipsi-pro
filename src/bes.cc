/*
    bes.cc: Implementation of getting Bessel array {J_i(x)}, i is from
            0 to n where J_{n+1}(x) < eps.

	Author: Kaixiang Huang <kxhuang@whu.edu.cn>.
    Copyright (c) 2019, Group Tipsi

    All rights reserved. Use of this source code is governed by a
    ???? license that can be found in the LICENSE file.
*/

#include "types.hh"
#include "math.hh"


namespace tipsi { namespace math {

RVector get_Bes(Real x, const Idx n_max)
{
    using constant::max_p;
    using constant::eps;

    Idx i, j;
    Real sum, two_div_x = 2 / x;
    RVector bes(n_max);

    i = n_max;
    bes[--i] = 0; // bes[n_max - 1]
    bes[--i] = 1; // bes[n_max - 2], and now i = n_max - 2

    while (i != 0) { // use the recurrence relation of Bessel function
        j = i--;  // j = i, i--
        bes[i] = two_div_x * j * bes[j] - bes[j + 1];

        if(bes[i] > max_p) { // cblas_dscal(n_max-i, eps, &bes[i], 1);
            for (Idx k = i; k < n_max; k++) bes[k] *= eps;
        }
    }

    sum = bes[0] * bes[0];
    #pragma omp parallel
    {
        #pragma omp for //simd
        for (Idx i = 1; i < n_max; i++) {
            sum += 2 * bes[i] * bes[i];
        }

        #pragma omp single
        {
            sum = sqrt(sum);
        }

        #pragma omp for //simd
        for (Idx i = 0; i < n_max; i++) {
            bes[i] /= sum; // now the values are right
        }
    }

    i = static_cast<Idx>(x);
    while(bes[++i] > eps);
    bes.resize(i); // now the size of bes is right

    return bes;
}

}} // namespace tipsi::math
