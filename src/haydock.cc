/*
    haydock.cc: Implement the function getting Haydock coefficients.
                This is used to calculate Green's function in LDOS.

    Author: Kaixiang Huang <kxhuang@whu.edu.cn>.
    Copyright (c) 2019, Group Tipsi.

    All rights reserved. Use of this source code is governed by a
    ???? license that can be found in the LICENSE file.
*/

#include "utils.hh"
#include "types.hh"
#include "math.hh"
#include "smatrix.hh"

namespace tipsi{ namespace internal {

// std::pair<CVector, RVector>
// get_Haydock(CVector& n1, const SMatrix& H_csr, Real H_rescale, Idx n_depth)
// {
//     using namespace math;

//     Idx n_wf = n1.size();
//     Real sum;
//     CVector n0(n_wf), n2(n_wf), coefa;
//     RVector coefb;

//     coefa[0] = 0;
//     sum = 0;

//     #pragma omp parallel
//     {
//     #ifdef __AVX__
//         #pragma omp for
//         for (Idx i = 0; i < n_wf; i += 2) {
//             __Complex t1 = &n1[i], t2;

//             n2[i] = H_rescale * mv_i(H_csr, n1, i);
//             n2[i+1] = H_rescale * mv_i(H_csr, n1, i+1);
//             t2.load(&n2[i]);

//             coefa[0] += (dot_conj(t1, t2)).sum();
//         }

//         #pragma omp for
//         for (Idx i = 0; i < n_wf; i += 2) {
//             __Complex t1 = &n1[i], t2 = &n2[i];

//             t2 -= t1 * coefa[0];
//             sum += dot_conj(t2);
//         }

//     #else
//         #pragma omp for
//         for(Idx i = 0; i < n_wf; i++)
//         {
//             n2[i] = H_rescale * mv_i(H_csr, n1, i);
//             coefa[0] += dot_conj(n1[i], n2[i]);
//         }

//         #pragma omp for
//         for(Idx i = 0; i < n_wf; i++)
//         {
//             n2[i] -= coefa[0] * n1[i];
//             sum += dot_conj(n2[i]);
//         }
//     #endif
//     }

//     coefb[0] = sqrt(sum); // norm of n2

//     for(Idx j = 1; j < n_depth; j++)
//     {
//         coefa[j] = 0;
//         sum = 0;

//         exchange(n0, n1, n2); // n0 <- n1, n1 <- n2, n2 <- (old)n0
//         #pragma omp parallel
//         {
//         #ifdef __AVX__
//             #pragma omp for
//             for(Idx i = 0; i < n_wf; i += 2)
//             {
//                 __Complex t1 = &n1[i], t2;

//                 t1 /= coefb[j - 1];
//                 n2[i] = H_rescale * mv_i(H_csr, n1, i);
//                 n2[i+1] = H_rescale * mv_i(H_csr, n1, i+1);
//                 t2.load(&n2[i]);

//                 coefa[0] += (dot_conj(t1, t2)).sum();
//             }

//             #pragma omp for
//             for(Idx i = 0; i < n_wf; i += 2)
//             {
//                 __Complex t0 = &n0[i], t1 = &n1[i], t2 = &n2[i];

//                 t2 -= t1 * coefa[j] + coefb[j - 1] * t0;
//                 sum += dot_conj(t2);
//             }

//         #else
//             #pragma omp for
//             for(Idx i = 0; i < n_wf; i++)
//             {
//                 n1[i] /= coefb[j - 1];
//                 n2[i] = H_rescale * mv_i(H_csr, n1, i);
//                 coefa[j] += dot_conj(n1[i], n2[i]);
//             }

//             #pragma omp for
//             for(Idx i = 0; i < n_wf; i++)
//             {
//                 n2[i] -= coefa[j] * n1[i] + coefb[j - 1] * n0[i];
//                 sum += dot_conj(n2[i]);
//             }
//         #endif
//         }

//         coefb[j] = sqrt(sum); // norm of n2
//     }
//     return std::make_pair(coefa, coefb);
// }

}} // namespace tipsi::internal
