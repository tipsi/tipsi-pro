/*
    utils.hh: Get some utility functions, Including memory operations,
              system time and standard input/output.

    Author: Kaixiang Huang <kxhuang@whu.edu.cn>
    Copyright (c) 2019, Group Tipsi

    All rights reserved. Use of this source code is governed by a
    ???? license that can be found in the LICENSE file.
*/

#pragma once

#include <utility>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <string>
#include <chrono>
#include "types.hh"


namespace tipsi
{

using std::make_pair;
using std::swap;
using std::move;
using std::string;


// compare whether two strings are equal with case insensative
inline bool iequals(const string& a, const string& b)
{
    using std::tolower;
    size_t sz = a.size();

    if (b.size() != sz) return false;
    for (size_t i = 0; i < sz; i++) {
        if (tolower(a[i]) != tolower(b[i])) return false;
    }
    return true;
}


// compare whether two strings are not equal with case insensative
inline bool inequals(const string& a, const string& b)
{
    using std::tolower;
    size_t sz = a.size();

    if (b.size() != sz) return true;
    for (size_t i = 0; i < sz; i++) {
        if (tolower(a[i]) != tolower(b[i])) return true;
    }
    return false;
}


inline bool stob(const string& str)
{
    if (iequals(str, "1") || iequals(str, "true")) return true;
    return false;
}


namespace time
{
    using time = std::chrono::system_clock;
    using timediff = std::chrono::duration<double>;
} // namespace time


namespace ios
{
    using std::cout;
    using std::endl;
    using std::flush;
    using std::ifstream;
    using std::ofstream;
    using std::istringstream;
    using std::setw;
    using std::getline;
} // namespace ios

} // namespace tipsi
