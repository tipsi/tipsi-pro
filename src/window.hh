#pragma once

#include "math.hh"


namespace tipsi {

enum Window {window_Hanning, window_exp, window_exp_10, window_exp_2};

inline RVector get_window(Window win, Idx n)
{
    using namespace math;

    RVector out(n);
    switch (win) {
        case window_Hanning:
            #pragma omp parallel for simd
            for (Idx i = 0; i < n; i++) {
                out[i] = 0.5 * (1 + cos(constant::PI * i / n));
            }
            break;
        case window_exp:
            #pragma omp parallel for simd
            for (Idx i = 0; i < n; i++) {
                Real temp = static_cast<Real>(i) / static_cast<Real>(n);
                out[i] = exp(-2 * temp * temp);
            }
            break;
        case window_exp_10:
            #pragma omp parallel for simd
            for (Idx i = 0; i < n; i++) {
                Real temp = static_cast<Real>(i) / static_cast<Real>(n);
                out[i] = pow(10, -2 * temp * temp);
            }
            break;
        case window_exp_2:
            #pragma omp parallel for simd
            for (Idx i = 0; i < n; i++) {
                Real temp = static_cast<Real>(i) / static_cast<Real>(n);
                out[i] = pow(2, -2 * temp * temp);
            }
            break;
        default:
            #pragma omp parallel for simd
            for (Idx i = 0; i < n; i++) {
                out[i] = 1;
            }
            break;
    }
    return out;
}

} // namespace tipsi
