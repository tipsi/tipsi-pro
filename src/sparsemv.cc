/*
    sparsemv.cc: Define sparse matrix-vector multiplication.
                 It is optimized on SSE2/AltiVec/NEON/ZVector architects.

    Author: Kaixiang Huang <kxhuang@whu.edu.cn>.
    Copyright (c) 2019, Group Tipsi.

    All rights reserved. Use of this source code is governed by a
    ???? license that can be found in the LICENSE file.
*/

#include "smatrix.hh"

namespace tipsi { namespace internal {

// out = mat * in
CVector operator*(const SMatrix& mat, const CVector& in)
{
    const Idx n_wf = in.size();
    CVector out(n_wf);
#ifdef MKL
    mkl_sparse_z_mv(SPARSE_OPERATION_NON_TRANSPOSE, 1, mat.sp_mat_mkl,
                    H_descr, in.data(), 0, out.data());
#else
    #pragma omp parallel for
    for (Idx i = 0; i < n_wf; i++) {
        // out[i] = 0;
        // #pragma omp simd
        for (Idx j = mat.indptr[i]; j < mat.indptr[i + 1]; j++) {
            out[i] += mat.values[j] * in[mat.indices[j]];
        }
    }
#endif
    return out;
}

// out = a * mat * in
CVector amv(const Complex& a, const SMatrix& mat, const CVector& in)
{
    const Idx n_wf = in.size();
    CVector out(n_wf);
#ifdef MKL
    mkl_sparse_z_mv(SPARSE_OPERATION_NON_TRANSPOSE, a, mat.sp_mat_mkl,
                    H_descr, in.data(), 0, out.data());
#else
    #pragma omp parallel for
    for (Idx i = 0; i < n_wf; i++) {
        // out[i] = 0;
        // #pragma omp simd
        for (Idx j = mat.indptr[i]; j < mat.indptr[i + 1]; j++) {
            out[i] += a * mat.values[j] * in[mat.indices[j]];
        }
    }
#endif
    return out;
}

// out = a * mat * in + out
void am_v1_p_v0(const Complex& a, const SMatrix& mat,
                const CVector& in, CVector& out)
{
#ifdef MKL
    mkl_sparse_z_mv(SPARSE_OPERATION_NON_TRANSPOSE, a, mat.sp_mat_mkl,
                    H_descr, in.data(), 1, out.data());
#else
    #pragma omp parallel for
    for (Idx i = 0; i < in.size(); i++) {
        // #pragma omp simd
        for (Idx j = mat.indptr[i]; j < mat.indptr[i + 1]; j++) {
            out[i] += a * mat.values[j] * in[mat.indices[j]];
        }
    }
#endif
}

}} // namespace tipsi::internal
