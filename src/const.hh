/*
    const.hh: Define some mathematical and physical constants globally.

    Author: Kaixiang Huang <kxhuang@whu.edu.cn>.
    Copyright (c) 2019, Group Tipsi.

    All rights reserved. Use of this source code is governed by a
    ???? license that can be found in the LICENSE file.
*/

#pragma once

#include "types.hh"


namespace tipsi { namespace constant {

    // mathematical constants
    static constexpr Real PI = 3.14159265358979323846264338328; // pi
    static constexpr Real max_p = 9007199254740992; // 2^53, machine precision
    static constexpr Complex img = Complex{0, 1}; // Imaginary unit I
    static constexpr Real eps = 1.1102230246251565404236316680908203125e-16;

    // physical constants
    static constexpr Real e = 1.602176634e-19; // elementary charge [C]

}} // namespace tipsi::constant
