/*
    smatrix.hh: Define a class for sparse csr matrix. This class can
                take advantage of multi-threading.

    Author: Kaixiang Huang <kxhuang@whu.edu.cn>.
    Copyright (c) 2019, Group Tipsi.

    All rights reserved. Use of this source code is governed by a
    ???? license that can be found in the LICENSE file.
*/

#pragma once

#include "types.hh"
#include "math.hh"

#ifdef MKL
    #include "mkl_spblas.h"
#endif


namespace tipsi {

namespace internal {

using IVector = vector<Idx>; // vector for Idx

// class used in sub-thread
class SMatrix_div {
    Idx count = 0;
    IVector indptr;
    IVector indices;
    CVector values;

public:
    explicit SMatrix_div(Idx n_hops, Idx chunk)
    {
        Idx temp = n_hops * chunk;
        indptr.reserve(chunk);
        indices.reserve(temp);
        values.reserve(temp);
    }

    void add(Idx idx, const Complex& val)
    {
        count++;
        indices.push_back(idx);
        values.push_back(val);
    }

    void next()
    {
        indptr.push_back(count);
    }

    friend class SMatrix;
};


#ifdef MKL
    static constexpr matrix_descr H_descr{SPARSE_MATRIX_TYPE_GENERAL,
                                          SPARSE_FILL_MODE_UPPER,
                                          SPARSE_DIAG_NON_UNIT};
#endif

// main class in serial
class SMatrix {
public:
    IVector indptr;
    IVector indices;
    CVector values;
#ifdef MKL
    sparse_matrix_t sp_mat_mkl;
#endif
private:
    const Idx n_order;
    Idx _size = 0;
    bool rescaled = false;
public:
    Real H_rescale = 0;

    explicit SMatrix(Idx n_wf, Idx n_hops): n_order(n_wf)
    {
        indptr.reserve(n_wf + 1);
        indptr.push_back(0);

        indices.reserve(n_hops);
        values.reserve(n_hops);
        }

    constexpr Idx n_wf() const { return n_order; }

    constexpr Idx size() const { return _size; }

    void concatenate(SMatrix_div& div) noexcept
    {
        for (auto& iptr: div.indptr) iptr += _size;
        indptr.insert(indptr.end(), div.indptr.begin(), div.indptr.end());
        indices.insert(indices.end(), div.indices.begin(), div.indices.end());
        values.insert(values.end(), div.values.begin(), div.values.end());
        _size += div.count;
    }

    SMatrix& rescale(Real H_re = 0)
    {
        using namespace math;
        if (rescaled) return *this;

        /* default method to determine H_rescale
         * !!! This could be very slow, so try to avoid it! */
        if (H_re == 0) {
            H_rescale = 0;
            for (Idx i = 0; i < n_order; i++) {
                Real sum = 0;
                #pragma omp parallel for reduction(+: sum)
                for (Idx j = indptr[i]; j < indptr[i+1]; j++) {
                    sum += abs(values[j]);
                }
                if (sum > H_rescale) H_rescale = sum;
            }
        }
        else H_rescale = H_re;

        // cblas_zdscal(_size, 1.0/H_rescale, values.data(), 1);
        #pragma omp for //simd
        for (Idx i = 0; i < _size; i++) {
            values[i] /= H_rescale;
        }
        rescaled = true;

        return *this;
    }

    SMatrix& unrescale()
    {
        if (!rescaled) return *this;

        cblas_zdscal(_size, H_rescale, values.data(), 1);
        rescaled = false;

        return *this;
    }

    void optimize(Idx n_calls) const
    {
    #ifdef MKL
        mkl_sparse_z_create_csr(const_cast<sparse_matrix_t *>(&sp_mat_mkl),
                                SPARSE_INDEX_BASE_ZERO, n_order, n_order,
                                const_cast<Idx*>(indptr.data()),
                                const_cast<Idx*>(&indptr[1]),
                                const_cast<Idx*>(indices.data()),
                                const_cast<Complex*>(values.data()));
        mkl_sparse_set_mv_hint(sp_mat_mkl, SPARSE_OPERATION_NON_TRANSPOSE,
                               H_descr, n_calls);
        mkl_sparse_set_memory_hint(sp_mat_mkl, SPARSE_MEMORY_AGGRESSIVE);
        mkl_sparse_optimize(sp_mat_mkl);
    #endif
    }

    // out = mat * in
    friend CVector operator*(const SMatrix& mat, const CVector& in);

    // out = a * mat * in
    friend CVector amv(const Complex& a, const SMatrix& mat, const CVector& in);

    // out = a * mat * in + out
    friend void am_v1_p_v0(const Complex& a, const SMatrix& mat,
                           const CVector& in, CVector& out);
};

} // namespace tipsi::internal

using internal::SMatrix;

} // namespace tipsi
