#pragma once

#include "sample.hh"
#include "tbpm.hh"
#include "window.hh"

namespace tipsi{

// struct for energy range
struct Energy {
    Real range;
    RVector energies;
    bool uniform = true;

    Energy() {}

    explicit Energy(Real min, Real max, Idx n_energies): range(max - min)
    {
        energies.resize(n_energies);
        #pragma omp simd
        for (Idx i = 0; i < n_energies; i++) {
            energies[i] = min + range * i / static_cast<Real>(n_energies);
        }
    }
};

// configurations
struct Config {
    bool l_bandstr = false;
    bool l_dos = false;
    Idx n_ran_samples = 1;
    Idx n_timestep = 1024;
    Idx band_resolution = 64;
    Idx seed = 1337;
    Real rescale = 0;
    Real en_cutoff;
    Loc3 n_max = {1, 1, 1};
    Bool3 pbc = {true, true, true};
    Energy energy;
};


// struct of band-structrue
struct Bandstr {
    RVector ktags;            // scalar values for high-symmetric tags
    RVector kvals;            // scalar values for all K-points
    vector<RVector> energies; // containing all eigenvalues

    void save() const;
};


struct Dos {
    const RVector& energies;
    RVector dos;

    explicit Dos(const Energy& eng)
    : energies(eng.energies), dos(eng.energies.size())
    {}

    void save() const;
};

Bandstr get_bandstr(const Lattice& lat, const HopDict& hops,
                    const Sites& momenta, Idx res = 64);

Dos get_dos(const Sample& sample, const Energy& energy, Idx seed,
            Idx n_timestep, Idx n_ran_samples, Window window = window_Hanning);

namespace input {

Lattice read_lattice(const string& file="LATTICE");
HopDict read_hopping(Real en_cutoff = 0, const string& file="HOPPINGS");
Config read_config(const string& file="CONFIG");
Sites read_kpoints(const string& file="KPOINTS");

} // namespace input

} // namespace tipsi
