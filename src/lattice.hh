/*
    lattice.hh: Define class Lattice, which means a unit cell.

    Author: Kaixiang Huang <kxhuang@whu.edu.cn>.
    Copyright (c) 2019, Group Tipsi.

    All rights reserved. Use of this source code is governed by a
    ???? license that can be found in the LICENSE file.
*/

#pragma once

#include "types.hh"
#include "const.hh"
#include "utils.hh"
#include "math.hh"


namespace tipsi {

class Lattice {
    const Matrix3 vectors;
    const Sites orbital_coords;
private:
    Real volumn = 0;

public:
    Lattice(Matrix3&& vec, Sites&& orb_coords, Real vol = 0)
    : vectors(vec), orbital_coords(orb_coords)
    {
        using namespace math;
        volumn = det(vectors);
    }

    Idx size() const { return orbital_coords.size(); }

    Site get_site(const Loc3& loc, Idx orb) const
    {
        Site coord;
        #pragma omp simd
        for (Idx i = 0; i < 3; i++) {
            coord[i] = orbital_coords[orb][i];
        }

        for (Idx i = 0; i < 3; i++) {
            #pragma omp simd
            for (Idx j = 0; j < 3; j++) {
               coord[i] += vectors(j, i) * loc[j];
            }
        }
        return coord;
    }

    Site get_dr(Idx orb0, const Rel3& rel, Idx orb1) const
    {
        Site coord;
        #pragma omp simd
        for (Idx i = 0; i < 3; i++) {
            coord[i] = orbital_coords[orb1][i] - orbital_coords[orb0][i];
        }

        for (Idx i = 0; i < 3; i++) {
            #pragma omp simd
            for (Idx j = 0; j < 3; j++) {
               coord[i] += vectors(j, i) * rel[j];
            }
        }
        return coord;
    }

    Matrix3 get_rec() const
    {
        Idx i1, i2, j1, j2;
        Matrix3 rec_vectors;

        // get reciprocal lattice
        Real twopi_div_volume = 2 * constant::PI / volumn;
        for (Idx i = 0; i < 3; i++) {
            i1 = (i + 1) % 3;
            i2 = (i + 2) % 3;
            for (Idx j = 0; j < 3; j++) {
                j1 = (j + 1) % 3;
                j2 = (j + 2) % 3;
                rec_vectors(i, j) =
                    twopi_div_volume * ( vectors(i1, j1) * vectors(i2, j2)
                                       - vectors(i1, j2) * vectors(i2, j1));
            }
        }
        return rec_vectors;
    }

};

} // namespace tipsi
