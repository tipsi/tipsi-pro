#pragma once

#include "types.hh"
#include "smatrix.hh"


namespace tipsi { namespace internal {

CVector cheb_wf_timestep_fwd(const CVector& wf_in, const RVector& Bes,
                             const SMatrix& H_csr);

CVector cheb_wf_timestep_inv(const CVector& wf_in, const RVector& Bes,
                             const SMatrix& H_csr);

CVector current_op(const CVector& wf_in, const RVector& cheb_coef,
                   const SMatrix& H_csr);

CVector tbpm_dos(const SMatrix& H_csr, Real rescaled_range, Idx seed,
                 Idx n_timestep, Idx n_ran_samples);

}} // namespace tipsi::internal
