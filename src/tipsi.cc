#include "tipsi.hh"

int main()
{
    using namespace tipsi;
    using namespace input;
    using namespace ios;

    std::ios_base::sync_with_stdio(false);

    Lattice     lat = read_lattice();
    Config   config = read_config();
    HopDict hopdict = read_hopping(config.en_cutoff);

    if (config.l_bandstr) {
        Sites high_sym = read_kpoints();
        Bandstr band = get_bandstr(lat, hopdict, high_sym,
                                   config.band_resolution);
        band.save();
        cout << endl;
    }

    cout << "Building sample" << endl;

    SiteSet sites{lat, move(config.n_max), move(config.pbc)};
    sites.get_tags_only();

    Sample sample(move(sites), hopdict.size());
    sample.get_hoppings(hopdict);
    sample.rescale(config.rescale);

    if (config.l_dos) {
        Dos dos = get_dos(sample, config.energy, config.seed, config.n_timestep,
                          config.n_ran_samples);
        dos.save();
        cout << endl;
    }
}
