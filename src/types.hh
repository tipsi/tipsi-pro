/*
    types.hh: Define some useful type aliases in tipsi.

    Author: Kaixiang Huang <kxhuang@whu.edu.cn>.
    Copyright (c) 2019, Group Tipsi.

    All rights reserved. Use of this source code is governed by a
    ???? license that can be found in the LICENSE file.
*/

#pragma once

#include "init.h"
#include <cstddef>
#include <complex>
#include <array>
#include <vector>
#include <functional>
#include <unordered_map>

namespace tipsi {

/* Alias some basic types. */
using Idx = std::uint64_t;            // type used for indexing
using Int = int16_t;                // signed integer type
using UInt = std::uint16_t;         // unsigned integer type
using Bool = bool;                  // boolean type
using Real = double;                // real type
using Complex = std::complex<Real>; // complex type
using std::string;                  // string type

} // namespace tipsi

#ifdef MKL
    #define MKL_ILP64                    // use ilp64 interface
    #define MKL_INT tipsi::Idx           // integer type in MKL
    #define MKL_Complex16 tipsi::Complex // complex type in MKL
    #include "mkl_types.h"
#else
    // #define CBLAS_INDEX tipsi::Idx //!!!cblas.h already defines it as size_t
    #define lapack_int tipsi::Idx                // integer type in lapack
    #define lapack_complex_double tipsi::Complex // complex type in lapack
#endif

namespace tipsi {

/* Declare container types and classes. */
using std::array;         // std::array
using std::vector;        // std::vector

using RVector = vector<Real>;    // vector for Real type
using CVector = vector<Complex>; // vector for Complex type
using Rel3 = std::array<Int, 3>; // vector for relative lattice
using Loc3 = array<UInt, 3>;     // vector for n_lattices in all direction
using Site = array<Real, 3>;     // vector of 3 doubles
using Sites = vector<Site>;      // vector of coordinates
using Bool3 = array<Bool, 3>;    // bools for pbc

// 3*3 col-major real matrix, column major
struct Matrix3: public array<Real, 9> {
    Real& operator()(Idx i, Idx j) noexcept
    {
        return this->operator[](i + 3*j);
    }

    const Real& operator()(Idx i, Idx j) const noexcept
    {
        return this->operator[](i + 3*j);
    }
};

// Hamiltonian matrix, column major
class Hamilton: public vector<Complex> {
    Idx n_rows;
    Idx n_cols;

public:
    Hamilton(Idx i, Idx j): n_rows(i), n_cols(j), vector<Complex>(i*j) {}

    Hamilton(Idx n): n_rows(n), n_cols(n), vector<Complex>(n*n) {}

    constexpr Idx rows() const { return n_rows; }

    constexpr Idx cols() const { return n_cols; }

    void reserve(Idx i, Idx j) { vector<Complex>::reserve(i*j); }

    void reserve(Idx n) { vector<Complex>::reserve(n*n); }

    Complex& operator()(Idx i, Idx j) noexcept
    {
        return this->operator[](i + n_cols*j);
    }

    const Complex& operator()(Idx i, Idx j) const noexcept
    {
        return this->operator[](i + n_cols*j);
    }
};

// Tag is used to point to another orbital
struct RelTag {
    Int dx   : 13;
    Int dy   : 13;
    Int dz   : 13;
    Idx _orb : 25;

    constexpr Rel3 rel() const
    {
        return Rel3{dx, dy, dz};
    }

    constexpr Idx orb() const
    {
        return _orb;
    }
};

inline RelTag make_tag(const Rel3& rel, Idx orb)
{
    return RelTag{rel[0], rel[1], rel[2], orb};
}


struct SiteTag {
    UInt dx : 16;
    UInt dy : 16;
    UInt dz : 16;
    UInt    :  0; // break up padding
    Idx _orb;

    constexpr Loc3 loc() const
    {
        return Loc3{dx, dy, dz};
    }

    constexpr Idx orb() const
    {
        return _orb;
    }
};

inline SiteTag make_tag(const Loc3& loc, Idx orb)
{
    return SiteTag{loc[0], loc[1], loc[2], orb};
}

} // namespace tipsi


namespace std {

// hash function for Tag type
template<> struct hash<tipsi::RelTag> {
    tipsi::Idx operator()(const tipsi::RelTag& tag) const
    {
        return reinterpret_cast<const tipsi::Idx&>(tag);
    }
};

template<> struct equal_to<tipsi::RelTag> {
    tipsi::Idx operator()(const tipsi::RelTag& tag1,
                          const tipsi::RelTag& tag2) const
    {
        return reinterpret_cast<const tipsi::Idx&>(tag1)
            == reinterpret_cast<const tipsi::Idx&>(tag2);
    }
};

} // namespace std


namespace tipsi {

using SDict = std::unordered_map<RelTag, Complex>; //

}
