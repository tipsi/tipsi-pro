/*
    bandstr.cc: get band structure of a lattice.

    Author: Kaixiang Huang <kxhuang@whu.edu.cn>.
    Copyright (c) 2019, Group Tipsi.

    All rights reserved. Use of this source code is governed by a
    ???? license that can be found in the LICENSE file.
*/

#include "math.hh"
#include "utils.hh"
#include "tipsi.hh"

namespace tipsi {

namespace { // anonymous namespace for helper data and helper functions

    Idx resolution;
    Idx n_high_sym;
    Idx n_kpoints;
    Sites kpoints;
    Matrix3 rec_vectors;

    inline Site get_dk(const Site& frac1, const Site& frac2)
    {
        Site dk;
        #pragma omp simd
        for (Idx i = 0; i < 3; i++) {
            dk[i] = frac2[i] - frac1[i];
        }

        return dk;
    }

    inline Site get_momentum(const Site& frac)
    {
        Site out{0, 0, 0};

        for (Idx i = 0; i < 3; i++) {
            //#pragma omp simd
            for (Idx j = 0; j < 3; j++) {
                out[i] += rec_vectors(j, i) * frac[j];
            }
        }
        return out;
    }

    inline Real get_k_dis(const Site& diff)
    {
        using namespace math;

        Site k_coord = get_momentum(diff);

        return sqrt(dot(k_coord, k_coord));
    }

    inline Site interpolate(const Site& k, const Site& diff, Idx j)
    {
        Site kpoint;
        #pragma omp simd
        for (Idx i = 0; i < 3; i++) {
            kpoint[i] = k[i] + diff[i] * j / resolution;
        }

        return kpoint;
    }

    void get_kpoints(Bandstr& bandstr, const Sites& momenta)
    {
        Idx i_div;
        Real k_dis, dk;
        Site diff;

        // interpolate K-points
        for (Idx i = 0; i < n_high_sym - 1; i++) {
            i_div = i * resolution;
            diff = get_dk(momenta[i], momenta[i + 1]);
            k_dis = get_k_dis(diff);
            bandstr.ktags[i + 1] = bandstr.ktags[i] + k_dis;
            dk = k_dis / resolution;

            // j = 0
            kpoints[i_div] = get_momentum(momenta[i]);
            bandstr.kvals[i_div] = bandstr.ktags[i];

            #pragma omp parallel for
            for (Idx j = 1; j < resolution; j++) {
                Site kpoint = interpolate(momenta[i], diff, j);
                kpoints[i_div + j] = get_momentum(kpoint);
                bandstr.kvals[i_div + j] = bandstr.ktags[i] + j * dk;
            }
        }
        bandstr.kvals[n_kpoints - 1] = bandstr.ktags[n_high_sym - 1];
        kpoints[n_kpoints - 1] = get_momentum(momenta[n_high_sym - 1]);
    }

} // anonymous namespace


Bandstr get_bandstr(const Lattice& lat, const HopDict& hops,
                    const Sites& momenta, Idx res)
{
    using namespace math;
    using constant::img;

    const Idx n_orbs = lat.size();
    Bandstr bandstr;

    rec_vectors = lat.get_rec();
    n_high_sym = momenta.size();
    resolution = res;
    n_kpoints = (n_high_sym - 1) * res + 1;
    kpoints.resize(n_kpoints);
    bandstr.ktags.resize(n_high_sym);
    bandstr.ktags[0] = 0;
    bandstr.kvals.resize(n_kpoints);

    ios::cout << " Calculating band-structure\n";
    get_kpoints(bandstr, momenta);

    bandstr.energies.resize(n_kpoints);
    for (Idx k = 0; k < n_kpoints; k++) {
        Hamilton Hk(n_orbs, n_orbs);

        #pragma omp parallel for
        for (Idx orb0 = 0; orb0 < n_orbs; orb0++) {
            const SDict& dict = hops[orb0];

            for (auto&& item: dict) {
                const Rel3& rel = item.first.rel();
                const Idx& orb1 = item.first.orb();
                const Complex& hop = item.second;

                Site dr = lat.get_dr(orb0, rel, orb1);
                Hk(orb0, orb1) += hop * exp(img * dot(kpoints[k], dr));
            }
        }
        bandstr.energies[k] = get_eigen(Hk);
    }
    return bandstr;
}

void Bandstr::save() const
{
    using namespace ios;
    ofstream file;
    file.open("BAND.csv");

    for (Idx k = 0, i = 0; k < kvals.size(); k++) {
        file << kvals[k];

        for (Idx j = 0; j < energies[0].size(); j++) {
            file << ',' << energies[k][j];
        }

        if (ktags[i] == kvals[k]) {
            file << ',' << "#tag" << i;
            i++;
        }
        file << '\n';
    }
    file.close();
    cout << " Band-structure was stored in 'BAND.csv'." << endl;
}

} // namespace tipsi
