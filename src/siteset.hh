/*
    siteset.hh: This class is aimed to set all the sites of a sample,
                including boundary conditions.

    Author: Kaixiang Huang <kxhuang@whu.edu.cn>.
    Copyright (c) 2019, Group Tipsi.

    All rights reserved. Use of this source code is governed by a
    ???? license that can be found in the LICENSE file.
*/

#pragma once

#include "types.hh"
#include "math.hh"
#include "lattice.hh"


namespace tipsi {

class SiteSet {
    Idx n_wf;
    vector<SiteTag> tags;
    Sites sites;
private:
    const Lattice& lattice;
    const Loc3 n_max;
    const Bool3 pbc_bool;
    Loc3 m;

public:
    SiteSet(const Lattice& lat, Loc3&& n, Bool3&& pbc)
    : lattice(lat), pbc_bool(pbc), n_max(n)
    {
        m[0] = lattice.size();
        m[1] = n_max[0] * m[0];
        m[2] = n_max[1] * m[1];
    }

    constexpr Idx size() const { return n_wf; }

    const SiteTag& tag(Idx i) const { return tags[i]; }

    const Site& site(Idx i) const { return sites[i]; }

    Idx get_idx(const Loc3& loc0, const RelTag& rel_tag) const
    {
        Rel3 rel = rel_tag.rel();
        Loc3 loc;
        for (Idx i = 0; i < 3; i++) {
            if (pbc_bool[i]) loc[i] = math::mod(loc0[i] + rel[i], n_max[i]);
            else {
                Int tmp = loc0[i] + rel[i];
                if (tmp < 0 || tmp > n_max[i]) return n_wf;
                loc[i] = tmp;
            }
        }
        return loc[0] * m[0] + loc[1] * m[1] + loc[2] * m[2] + rel_tag.orb();
    }

    Site get_dr(const Idx& orb, const RelTag& rel_tag) const
    {
        const Rel3& rel = rel_tag.rel();
        const Idx& orb1 = rel_tag.orb();

        return lattice.get_dr(orb, rel, orb1);
    }

    // only get tags without coordinates
    void get_tags_only();

    // get tags together with coordinates
    void get_sites();
};

} // namespace tipsi
