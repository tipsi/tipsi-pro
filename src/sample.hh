#pragma once

#include "hopdict.hh"
#include "siteset.hh"
#include "smatrix.hh"


namespace tipsi {

namespace internal {

CVector random_state(Idx n_wf, Idx seed); // implemented in random.cc

} // namespace tipsi::internal

struct Sample {
    SMatrix H_csr;
private:
    const Idx n_wf;
    const SiteSet& siteset;
public:
    Sample(SiteSet&& site, Idx n_hops)
    : n_wf(site.size()), H_csr(site.size(), n_hops), siteset(site) {}

    void get_hoppings(const HopDict& hopdict);

    void rescale(Real H_rescale)
    {
        H_csr.rescale(H_rescale);
    }
};

} // namespace tipsi
