/*
    hopdict.hh: Define a dictionary containing loc -> hopping pair.

    Author: Kaixiang Huang <kxhuang@whu.edu.cn>.
    Copyright (c) 2019, Group Tipsi.

    All rights reserved. Use of this source code is governed by a
    ???? license that can be found in the LICENSE file.
*/

#pragma once

#include "types.hh"
#include "utils.hh"


namespace tipsi {

class HopDict {
    vector<SDict> dict;

public:
    explicit HopDict(Idx n): dict(n) {}

    Idx size() const
    {
        Idx count = 0;
        for (auto&& item: dict) {
            count += item.size();
        }
        return count;
    }

    const SDict& operator[](Idx n) const { return dict[n]; }

    void add(const Rel3& rel, Idx orb0, Idx orb1, const Complex& hop)
    {
        dict[orb0].insert(make_pair(make_tag(rel, orb1), hop));
    }
};

} // namespace tipsi
