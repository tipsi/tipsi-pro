#include "siteset.hh"

namespace tipsi{

void SiteSet::get_tags_only()
{
    const Idx n_orbs = lattice.size();
    n_wf = n_max[0] * n_max[1] * n_max[2] * n_orbs;

    tags.reserve(n_wf);

    for (UInt z = 0; z < n_max[2]; z++) {
        for (UInt y = 0; y < n_max[1]; y++) {
            for (UInt x = 0; x < n_max[0]; x++) {
                for (Idx orb = 0; orb < n_orbs; orb++) {
                    const Loc3& loc = {x, y, z};
                    SiteTag&& tag = make_tag(loc, orb);
                    tags.push_back(tag);
                }
            }
        }
    }
}


void SiteSet::get_sites()
{
    const Idx n_orbs = lattice.size();
    n_wf = n_max[0] * n_max[1] * n_max[2] * n_orbs;

    tags.reserve(n_wf);

    sites.reserve(n_wf);
    for (UInt z = 0; z < n_max[2]; z++) {
        for (UInt y = 0; y < n_max[1]; y++) {
            for (UInt x = 0; x < n_max[0]; x++) {
                for (Idx orb = 0; orb < n_orbs; orb++) {
                    const Loc3& loc = {x, y, z};
                    SiteTag&& tag = make_tag(loc, orb);
                    Site&& site_r = lattice.get_site(loc, orb);
                    tags.push_back(tag);
                    sites.push_back(site_r);
                }
            }
        }
    }
}

} // namespace tipsi
