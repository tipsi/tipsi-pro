#include "tipsi.hh"
#include "utils.hh"

namespace tipsi {

Dos get_dos(const Sample& sample, const Energy& energy, Idx seed,
            Idx n_timestep, Idx n_ran_samples, Window window)
{
    using namespace internal;
    using namespace math;

    Real rescaled_range = energy.range / sample.H_csr.H_rescale;
    Dos out(energy);
    ios::cout << " Calculating DOS correlation function\n";
    CVector corr_DOS = tbpm_dos(sample.H_csr, rescaled_range,
                                seed, n_timestep, n_ran_samples);

    const Idx n_energies = energy.energies.size();
    CVector corr_all(n_energies); // include both positive and negative time
    CVector corr_fft(n_energies); // corr after fft
    RVector win = get_window(window, n_timestep);

    corr_all[n_timestep - 1] = 1;
    corr_all[2*n_timestep-1] = win[n_timestep-1] * corr_DOS[n_timestep-1];

    #pragma omp parallel for //simd
    for (Idx i = 0; i < n_timestep - 1; i++) {
        Complex temp = win[i] * corr_DOS[i];

        corr_all[n_timestep + i] = temp;
        corr_all[n_timestep - 2 - i] = conj(temp);
    }

    corr_fft = fft1d_c2c(corr_all, 1);

    #pragma omp parallel for
    for (Idx i = 0; i < n_timestep; i++) {
        out.dos[i + n_timestep] = abs(corr_fft[i]);
        out.dos[i] = abs(corr_fft[i + n_timestep]);
    }

    Real sum = norm(out.dos) * 0.5 * energy.range / n_timestep;
    #pragma omp parallel for //simd
    for (Idx i = 0; i < n_energies; i++) {
        out.dos[i] /= sum; // normalise
    }
    return out;
}


void Dos::save() const
{
    using namespace ios;

    ofstream file;
    file.open("DOS.csv");
    file << std::setprecision(12);

    for(Idx i = 0; i < energies.size(); i++) {
        file << setw(15) << energies[i] << ',' << setw(15) << dos[i] << '\n';
    }
    file.close();
    cout << " DOS was stored in 'DOS.csv'." << endl;
}

} // namespace tipsi
