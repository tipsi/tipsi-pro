#include "tipsi.hh"
#include "utils.hh"

namespace tipsi { namespace input {

Lattice read_lattice(const string& file)
{
    using namespace ios;

    ifstream in;
    string line;
    istringstream iss;
    Matrix3 vec;
    Idx n_orbs;

    in.open(file);
    cout << " Reading lattice information from file '" << file << "'" << endl;

    // read first 3 lines for vectors
    for (Idx i = 0; i < 3; i++) {
        getline(in, line);
        iss.str(line);
        iss.clear();
        iss >> vec(i, 0) >> vec(i, 1) >> vec(i, 2);
    }

    // get rid of a blank line
    getline(in, line);

    //read n_orbs
    getline(in, line);
    iss.str(line);
    iss.clear();
    iss >> n_orbs;
    Sites orbs;

    orbs.reserve(n_orbs);
    // read orbital coordinates
    for (Idx i = 0; i < n_orbs; i++) {
        Site orb;

        getline(in, line);
        iss.str(line);
        iss.clear();
        iss >> orb[0] >> orb[1] >> orb[2];

        orbs.push_back(orb);
    }
    in.close();

    return Lattice{move(vec), move(orbs)};
}


HopDict read_hopping(Real en_cutoff, const string& file)
{
    using namespace ios;

    ifstream in;
    string line;
    istringstream iss;
    Idx n_orbs, n_hops;

    in.open(file);
    cout << " Reading hoppings from file '" << file << "'" << endl;

    // read n_orbs
    getline(in, line);
    iss.str(line);
    iss.clear();
    iss >> n_orbs;

    // read n_hops per unit cell
    getline(in, line);
    iss.str(line);
    iss.clear();
    iss >> n_hops;

    HopDict hops(n_orbs);

    // read the hoppings
    while (n_hops--) {
        Rel3 rel;
        Idx orb0, orb1;
        Real re, im;
        Complex hop;

        getline(in, line);
        iss.str(line);
        iss.clear();
        iss >> rel[0] >> rel[1] >> rel[2] >> orb0 >> orb1 >> re >> im;
        hop = Complex(re, im);

        if (abs(hop) > en_cutoff) {
            hops.add(rel, orb0, orb1, hop);
        }
    }
    in.close();

    return hops;
}


Sites read_kpoints(const string& file)
{
    using namespace ios;

    ifstream in;
    string line;
    istringstream iss;
    Idx n_high_sym;

    in.open(file);
    cout << " Reading kpoints from file '" << file << "'" << endl;

    // read n_high_sym
    getline(in, line);
    iss.str(line);
    iss.clear();
    iss >> n_high_sym;

    Sites high_sym(n_high_sym);

    // read the kpoints
    for (Idx i = 0; i < n_high_sym; i++) {
        Site kpoint;

        getline(in, line);
        iss.str(line);
        iss.clear();
        iss >> kpoint[0] >> kpoint[1] >> kpoint[2];

        high_sym[i] = kpoint;
    }
    in.close();

    return high_sym;
}


Config read_config(const string& file)
{
    using namespace ios;

    ifstream in;
    string line, arg, _;
    Real en_min, en_max;
    istringstream iss;
    Config config;

    in.open(file);
    cout << " Reading config from file '" << file << "'" << endl;

    while (getline(in, line)) {
        if (line.empty()) continue; // get rid of blank lines
        iss.str(line);
        iss.clear();
        iss >> arg >> _;

        if (iequals(arg, "lband")) {
            string bool_str;
            iss >> bool_str;
            config.l_bandstr = stob(bool_str);
            continue;
        }

        if (iequals(arg, "ldos")) {
            string bool_str;
            iss >> bool_str;
            config.l_dos = stob(bool_str);
            continue;
        }

        if (iequals(arg, "n_samples")) {
            iss >> config.n_ran_samples;
            continue;
        }

        if (iequals(arg, "n_timestep")) {
            iss >> config.n_timestep;
            continue;
        }

        if (iequals(arg, "band_res")) {
            iss >> config.band_resolution;
            continue;
        }

        if (iequals(arg, "rescale")) {
            iss >> config.rescale;
            continue;
        }

        if (iequals(arg, "en_cutoff")) {
            iss >> config.en_cutoff;
            continue;
        }

        if (iequals(arg, "n_max")) {
            iss >> config.n_max[0] >> config.n_max[1] >> config.n_max[2];
            continue;
        }

        if (iequals(arg, "pbc")) {
            string p0, p1, p2;
            iss >> p0 >> p1 >> p2;
            config.pbc[0] = stob(p0);
            config.pbc[1] = stob(p1);
            config.pbc[2] = stob(p2);
            continue;
        }

        if (iequals(arg, "en_range")) {
            iss >> en_min >> en_max;
            continue;
        }
    }
    in.close();

    Energy en_range{en_min, en_max, 2*config.n_timestep};
    config.energy = move(en_range);

    return config;
}

}} // namespace tipsi::input
