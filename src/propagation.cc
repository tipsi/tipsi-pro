/*
    propagation.cc: Implement the propagation of TDSE and Fermi-Dirac operator
                    using Chebshev polynomial recurrence.

    Author: Kaixiang Huang <kxhuang@whu.edu.cn>.
    Copyright (c) 2019, Group Tipsi.

    All rights reserved. Use of this source code is governed by a
    ???? license that can be found in the LICENSE file.
*/

#include "utils.hh"
#include "types.hh"
#include "smatrix.hh"
#include "math.hh"


namespace tipsi { namespace internal {
/* Apply forward timestep using Chebyshev decomposition. */
CVector cheb_wf_timestep_fwd(const CVector& wf_in, const RVector& Bes,
                             const SMatrix& H_csr)
{
    using constant::img;
    using namespace math;

    const Idx n_bes = Bes.size(), n_wf = wf_in.size();
    CVector Tcheb0, Tcheb1, out(n_wf);

    Tcheb0 = copy(wf_in);
    Tcheb1 = amv(-img, H_csr, Tcheb0);

    #pragma omp parallel for //simd
    for (Idx i = 0; i < n_wf; i++) {
        out[i] = Bes[0] * Tcheb0[i] + 2 * Bes[1] * Tcheb1[i];
    }

    for (Idx j = 2; j < n_bes; j++) {
        am_v1_p_v0(-2.0*img, H_csr, Tcheb1, Tcheb0);
        axpy(2*Bes[j], Tcheb0, out);

        swap(Tcheb0, Tcheb1);
    }
    return out;
}

/* Apply inverse timestep using Chebyshev decomposition. */
CVector cheb_wf_timestep_inv(const CVector& wf_in, const RVector& Bes,
                             const SMatrix& H_csr)
{
    using constant::img;
    using namespace math;

    const Idx n_bes = Bes.size(), n_wf = wf_in.size();
    CVector Tcheb0, Tcheb1, out(n_wf);

    Tcheb0 = copy(wf_in);
    Tcheb1 = amv(img, H_csr, wf_in);

    #pragma omp parallel for //simd
    for (Idx i = 0; i < n_wf; i++) {
        out[i] = Bes[0] * Tcheb0[i] + Bes[1] * Tcheb1[i];
    }

    for (Idx j = 2; j < n_bes; j++) {
        am_v1_p_v0(2.0*img, H_csr, Tcheb1, Tcheb0);
        axpy(2*Bes[j], Tcheb0, out);

        swap(Tcheb0, Tcheb1);
    }
    return out;
}

}} // namespace tipsi::internal
