/*
    math.hh: Define wrappers for some frequently used mathematical functions.

    Author: Kaixiang Huang <kxhuang@whu.edu.cn>.
    Copyright (c) 2019, Group Tipsi.

    All rights reserved. Use of this source code is governed by a
    ???? license that can be found in the LICENSE file.
*/

#pragma once

#include <cmath>
#include "types.hh"
#include "const.hh"

#ifdef MKL
    #include <mkl_cblas.h>
    #include <mkl_lapacke.h>
    #include <fftw/fftw3_mkl.h> // mkl dfti with fftw interface
#else
    #include <cblas.h>
    #include <lapacke.h>
    #include <fftw3.h>
#endif


namespace tipsi { namespace math {

using std::sin;
using std::cos;
using std::abs;
using std::exp;
using std::pow;
using std::sqrt;
using std::conj;


// modulo function that always return positive number
constexpr UInt mod(Int i, Int n) { return i < 0 ? i % n + n : i % n; }

inline Real dot(const Site& a, const Site& b)
{
    Real out = 0;
    //#pragma omp simd
    for (Idx i = 0; i < 3; i++) {
        out += a[i] * b[i];
    }
    return out;
}


// determinant of a Matrix3, which means mix product of three Sites
inline Real det(const Matrix3& mat)
{
    return mat[0] * mat[4] * mat[8] + mat[3] * mat[7] * mat[2]
         + mat[6] * mat[1] * mat[5] - mat[0] * mat[7] * mat[5]
         - mat[3] * mat[1] * mat[8] - mat[6] * mat[4] * mat[2];
}


/******************* BLAS and LAPACK interfaces ******************/
// get norm of a real vector
inline Real norm(const RVector& in)
{
    return cblas_dnrm2(in.size(), in.data(), 1);
}


// get norm of a complex vector
inline Real norm(const CVector& in)
{
    return cblas_dznrm2(in.size(), in.data(), 1);
}


inline CVector copy(const CVector& from)
{
    Idx n = from.size();
    CVector to(n);

    cblas_zcopy(n, from.data(), 1, to.data(), 1);
    return to;
}


inline void axpy(Real a, const CVector& x, CVector& y)
{
    Complex temp = Complex(a, 0);
    cblas_zaxpy(x.size(), &temp, x.data(), 1, y.data(), 1);
}


// inner_prod of oneself
inline Real inner_prod(const CVector& in)
{
    Real temp = norm(in);
    return temp * temp;
}


// get inner_prod as sum_i(conj(a[i]) * b[i]) with same length
inline Complex inner_prod(const CVector& a, const CVector& b)
{
    CBLAS_INDEX n = a.size();
    Complex out;

    cblas_zdotc_sub(n, a.data(), 1, b.data(), 1, &out);

    return out;
}


/* Get accending eigenvalues of a Hamiltonian. Use LAPACK if possible. */
inline RVector get_eigen(const Hamilton& Hk)
{
    lapack_int n = Hk.rows();
    RVector out(n);
    lapack_complex_double* ptr = const_cast<Complex*>(Hk.data());

    LAPACKE_zheev(LAPACK_COL_MAJOR, 'N', 'L', n, ptr, n, out.data());

    return out;
}

/*********************** FFTW interfaces **************************/
static const unsigned flag = FFTW_ESTIMATE|FFTW_PRESERVE_INPUT;

inline RVector fft1d_r2r(const RVector& in, Idx n_half_p_1)
{
    RVector out(n_half_p_1);
    int n = static_cast<int>(n_half_p_1);
    Real* in_ptr = const_cast<Real*>(in.data());
    Real* out_ptr = out.data();

    fftw_plan p = fftw_plan_r2r_1d(n, in_ptr, out_ptr, FFTW_REDFT00, flag);
    fftw_execute(p);
    fftw_destroy_plan(p);

    return out;
}


inline CVector fft1d_c2c(const CVector& in, int direction)
{
    int n = in.size();
    CVector out(n);
    fftw_complex* in_ptr = reinterpret_cast<fftw_complex*>(
                            const_cast<Complex*>(in.data()));
    fftw_complex* out_ptr = reinterpret_cast<fftw_complex*>(out.data());

    fftw_plan p = fftw_plan_dft_1d(n, in_ptr, out_ptr, direction, flag);
    fftw_execute(p);
    fftw_destroy_plan(p);
    return out;
}


/*** Get Bessel array {J_i(x)}, i is from 0 to n where J_{n+1}(x) < eps. ***/
RVector get_Bes(Real x, const Idx n_max = 1024); // implemented in bes.cc

}} // namespace tipsi::math
