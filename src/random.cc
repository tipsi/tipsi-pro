/*
    random.cc: This is the implementation of random wavefunction generator.

    Author: Kaixiang Huang <kxhuang@whu.edu.cn>.
    Copyright (c) 2019, Group Tipsi.

    All rights reserved. Use of this source code is governed by a
    ???? license that can be found in the LICENSE file.
*/

#include "types.hh"
#include "const.hh"
#include "math.hh"
#include <random>

#ifdef MKL
    #include <mkl_vsl.h>
#endif


namespace tipsi { namespace internal {

CVector random_state(Idx n_wf, Idx seed)
{
    using constant::PI;
    using namespace math;
    using constant::img;

    RVector square(n_wf), phase(n_wf);
#ifdef MKL
    VSLStreamStatePtr stream;

    vslNewStream(&stream, VSL_BRNG_SFMT19937, seed*45659);
    vdRngExponential(VSL_RNG_METHOD_EXPONENTIAL_ICDF, stream, n_wf,
                     square.data(), 0, 1);
    vdRngUniform(VSL_RNG_METHOD_UNIFORM_STD, stream, n_wf,
                 phase.data(), 0, 2*PI);
    vslDeleteStream(&stream);
#else
    std::mt19937_64 stream{seed*45659};
    std::exponential_distribution<Real> dis1{1};
    std::uniform_real_distribution<Real> dis2{0, 2*PI};

    for (Idx i = 0; i < n_wf; i++) {
        square[i] = dis1(stream);
        phase[i] = dis2(stream);
    }
#endif
    CVector out(n_wf);
    Real div = sqrt(cblas_dasum(n_wf, square.data(), 1));

    #pragma omp parallel for
    for (Idx i = 0; i < n_wf; i++) {
        out[i] = sqrt(square[i]) * exp(img * phase[i]) / div;
    }
    return out;
}

}} // namespace tipsi::internal
